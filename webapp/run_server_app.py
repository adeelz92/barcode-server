from flask import Flask, render_template, request
from flask_uploads import UploadSet, IMAGES, configure_uploads
from main import run
from waitress import serve

app = Flask(__name__)

photos = UploadSet('photos', IMAGES)
app.config['UPLOADED_PHOTOS_DEST'] = './Images'
configure_uploads(app, photos)

@app.route("/predict", methods=['GET', 'POST'])
def predict():
    if request.method == 'POST' and 'photo' in request.files:
        filename = photos.save(request.files['photo'])
        digits, color, time = run("Images/" + filename)
        return ''.join(digits) + ', ' + ''.join(color) + ', ' + str(round(time)) + 'sec'
    return render_template('upload.html')


if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=5090)